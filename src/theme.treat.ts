import { createTheme } from 'treat';

const themeValues = {
  brandColor: 'blue',
  grid: 4,
};

export type ThemeValues = typeof themeValues;

export default createTheme(themeValues);
