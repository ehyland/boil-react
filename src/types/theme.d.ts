declare module 'treat/theme' {
  export type ThemeValues = import('../theme.treat').ThemeValues;
  export interface Theme extends ThemeValues {}
}
