import React from 'react';
import { TreatProvider } from 'react-treat';
import theme from '../../theme.treat';
import * as styles from './App.treat';

const App = () => (
  <TreatProvider theme={theme}>
    <div className={styles.button}>
      <h1>Welcome to your app</h1>
    </div>
  </TreatProvider>
);

export default App;
