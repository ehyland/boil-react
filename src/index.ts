import './index.css';

import { createElement } from 'react';
import { render } from 'react-dom';
import App from './containers/App';

const rootEl = document.getElementById('root') as HTMLElement;

render(createElement(App, {}), rootEl);
